//
//  Chain.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/22/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import UIKit

class Chain: Hashable, CustomStringConvertible {
    
    var cookies = [Cookie]()
    var score = 0
    
    enum ChainType: CustomStringConvertible {
        case Horizontal
        case Vertical
        
        var description: String {
            switch self {
            case .Horizontal: return "Horizontal"
            case .Vertical: return "Vertical"
            }
        }
    }
    
    var chainType: ChainType
    
    init(chainType: ChainType) {
        self.chainType = chainType
    }
    
    func addCookie(cookie: Cookie) {
        cookies.append(cookie)
    }
    
    func firstCookie() -> Cookie {
        return cookies[0]
    }
    
    func lastCookie() -> Cookie {
        return cookies[cookies.count - 1]
    }
    
    var length: Int {
        return cookies.count
    }
    
    // MARK: CustomStringConvertible
    
    var description: String {
        return "type: \(chainType) cookies: \(cookies)"
    }
    
    // MARK: Hashable
    
    var hashValue: Int {
        return cookies.reduce(0) {
            $0.hashValue ^ $1.hashValue
        }
    }
    
}

// MARK: Hashable

func ==(lhs: Chain, rhs: Chain) -> Bool {
    return lhs.cookies == rhs.cookies
}
