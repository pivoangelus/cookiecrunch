//
//  Constants.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/16/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import SpriteKit

// work around to geting rid of 7.x bug saying CUICatalog can not resolve an idiom for the images
let GAMEATLAS = SKTextureAtlas(named: "Game")
let SPRITEATLAS = SKTextureAtlas(named: "Sprites")
let GRIDATLAS = SKTextureAtlas(named: "Grid")

// max grid size
let NUMCOLUMNS = 9
let NUMROWS = 9
