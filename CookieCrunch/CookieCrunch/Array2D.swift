//
//  Array2D.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/16/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//
//  array[a][b]
//

struct Array2D<T> {
    
    // MARK: Properities
    
    let columns: Int
    let rows: Int
    
    private var array: Array<T?>
    
    // MARK:
    
    init(columns: Int, rows: Int) {
        self.columns = columns
        self.rows = rows
        array = Array<T?>(count: rows*columns, repeatedValue: nil)
    }
    
    // create a pseudo [][]
    
    subscript(column: Int, row: Int) -> T? {
        get {
            return array[row * columns + column]
        }
        set {
            array[row * columns + column] = newValue
        }
    }
    
}