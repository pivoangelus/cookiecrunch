//
//  Cookie.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/16/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import SpriteKit

enum CookieType: Int, CustomStringConvertible {
    
    case Unknown = 0,
         Croissant,     // 1
         Cupcake,       // 2
         Danish,        // 3
         Donut,         // 4
         Macaroon,      // 5
         SugarCookie    // 6
    
    var spriteName: String {
        let spriteNames = [
            "Croissant",
            "Cupcake",
            "Danish",
            "Donut",
            "Macaroon",
            "SugarCookie"
        ]
        
        return spriteNames[rawValue - 1]
    }
    
    var highlightedSpriteName: String {
        return spriteName + "-Highlighted"
    }
    
    static func random() -> CookieType {
        return CookieType(rawValue: Int(arc4random_uniform(6)) + 1)!
    }
    
    // MARK: CustomStringConvertible
    
    var description: String {
        return spriteName
    }
    
}

class Cookie: CustomStringConvertible, Hashable {
    
    // MARK: Properities
    
    var column: Int
    var row: Int
    let cookieType: CookieType
    var sprite: SKSpriteNode?
    
    init(column: Int, row: Int, cookieType: CookieType) {
        self.column = column
        self.row = row
        self.cookieType = cookieType
    }
 
    // MARK: CustomStringConvertible
    
    var description: String {
        return "type: \(cookieType) square: (\(column),\(row))"
    }
    
    // MARK: Hashable
    
    var hashValue: Int {
        return row * 10 + column
    }
    
}

// MARK: Hashable

func ==(lhs: Cookie, rhs: Cookie) -> Bool {
    return lhs.column == rhs.column && lhs.row == rhs.row
}
