//
//  Level.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/16/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import UIKit

class Level {
    
    // MARK: Properities
    
    private var tiles = Array2D<Tile>(columns: NUMCOLUMNS, rows: NUMROWS)
    private var cookies = Array2D<Cookie>(columns: NUMCOLUMNS, rows: NUMROWS)
    private var possibleSwaps = Set<Swap>()
    private var comboMultiplier = 1
    
    var targetScore = 0
    var maximumMoves = 0
    
    // MARK:
    
    init(filename: String) {
        if let dictionary = Dictionary<String, AnyObject>.loadJSONFromBundle(filename) {
            if let tilesArray: AnyObject = dictionary["tiles"] {
                for (row, rowArray) in (tilesArray as! [[Int]]).enumerate() {
                    // game table
                    print("row: \(row), rowArray: \(rowArray)")
                    let tileRow = NUMROWS - row - 1
                    for (column, value) in rowArray.enumerate() {
                        if value == 1 {
                            tiles[column, tileRow] = Tile()
                        }
                    }
                    targetScore = dictionary["targetScore"] as! Int
                    maximumMoves = dictionary["moves"] as! Int
                }
            }
        }
    }
    
    func cookieAt(column column: Int, row: Int) -> Cookie? {
        assert(column >= 0 && column < NUMCOLUMNS)
        assert(row >= 0 && row < NUMROWS)
        return cookies[column, row]
    }
    
    func tileAt(column column: Int, row: Int) -> Tile? {
        assert(column >= 0 && column < NUMCOLUMNS)
        assert(row >= 0 && row < NUMROWS)
        return tiles[column, row]
    }
    
    func shuffle() -> Set<Cookie> {
        var set: Set<Cookie>
        
        repeat {
            set = createInitialCookies()
            detectPossibleSwaps()
            print("possible swaps: \(possibleSwaps)")
        }
        while possibleSwaps.count == 0
        
        return set
    }
    
    private func hasChain(column column: Int, row: Int) -> Bool {
        let cookieType = cookies[column, row]!.cookieType
        
        var horzLength = 1
        for var i = column - 1; i >= 0 && cookies[i, row]?.cookieType == cookieType; --i, ++horzLength {}
        for var i = column + 1; i < NUMCOLUMNS && cookies[i, row]?.cookieType == cookieType; ++i, ++horzLength {}
        if horzLength >= 3 { return true }
        
        var vertLength = 1
        for var i = row - 1; i >= 0 && cookies[column, i]?.cookieType == cookieType; --i, ++vertLength {}
        for var i = row + 1; i < NUMROWS && cookies[column, i]?.cookieType == cookieType; ++i, ++vertLength {}
        return vertLength >= 3
    }
    
    private func createInitialCookies() -> Set<Cookie> {
        var set = Set<Cookie>()
        
        for row in 0..<NUMROWS {
            for column in 0..<NUMCOLUMNS {
                if tiles[column, row] != nil {
                    var cookieType: CookieType
                    
                    repeat {
                        cookieType = CookieType.random()
                    }
                    while (column >= 2 &&
                           cookies[column - 1, row]?.cookieType == cookieType &&
                           cookies[column - 2, row]?.cookieType == cookieType) ||
                          (row >= 2 &&
                           cookies[column, row - 1]?.cookieType == cookieType &&
                           cookies[column, row - 2]?.cookieType == cookieType)
                    
                    let cookie = Cookie(column: column, row: row, cookieType: cookieType)
                    cookies[column, row] = cookie
                    set.insert(cookie)
                }
            }
        }
        return set
    }
    
    func performSwap(swap: Swap) {
        let columnA = swap.cookieA.column
        let rowA = swap.cookieA.row
        let columnB = swap.cookieB.column
        let rowB = swap.cookieB.row
        
        cookies[columnA, rowA] = swap.cookieB
        swap.cookieB.column = columnA
        swap.cookieB.row = rowA
        
        cookies[columnB, rowB] = swap.cookieA
        swap.cookieA.column = columnB
        swap.cookieA.row = rowB
    }
    
    func isPossibleSwap(swap: Swap) -> Bool {
        return possibleSwaps.contains(swap)
    }
    
    func fillHoles() -> [[Cookie]] {
        var columns = [[Cookie]]()
        
        for column in 0..<NUMCOLUMNS {
            var array = [Cookie]()
            for row in 0..<NUMROWS {
                if tiles[column, row] != nil && cookies[column, row] == nil {
                    for lookup in (row + 1)..<NUMROWS {
                        if let cookie = cookies[column, lookup] {
                            cookies[column, lookup] = nil
                            cookies[column, row] = cookie
                            cookie.row = row
                            array.append(cookie)
                            break
                        }
                    }
                }
                if !array.isEmpty {
                    columns.append(array)
                }
            }
        }
        return columns
    }
    
    func topOffCookies() -> [[Cookie]] {
        var columns = [[Cookie]]()
        var cookieType: CookieType = .Unknown
        
        for column in 0..<NUMCOLUMNS {
            var array = [Cookie]()
            for var row = NUMROWS - 1; row >= 0 && cookies[column, row] == nil; --row {
                if tiles[column, row] != nil {
                    var newCookieType: CookieType
                    repeat {
                        newCookieType = CookieType.random()
                    }
                    while newCookieType == cookieType
                    cookieType = newCookieType
                    let cookie = Cookie(column: column, row: row, cookieType: cookieType)
                    cookies[column, row] = cookie
                    array.append(cookie)
                }
            }
            if !array.isEmpty {
                columns.append(array)
            }
        }
        return columns
    }
    
    private func calculateScores(chains: Set<Chain>) {
        // 3-chain is 60 pts, 4-chain is 120, 5-chain is 180 ...
        for chain in chains {
            chain.score = 60 * (chain.length - 2) * comboMultiplier
            ++comboMultiplier
        }
    }
    
    func resetComboMultiplier() {
        comboMultiplier = 1
    }
    
    // MARK: Remove
    
    func removeMatches() -> Set<Chain> {
        let horizontalChains = detectHorizontalMatches()
        let verticalChains = detectVerticalMatches()
        
        print("H. matches: \(horizontalChains)")
        print("V. matches: \(verticalChains)")
        
        removeCookies(horizontalChains)
        removeCookies(verticalChains)
        
        calculateScores(horizontalChains)
        calculateScores(verticalChains)
        
        return horizontalChains.union(verticalChains)
    }
    
    func removeCookies(chains: Set<Chain>) {
        for chain in chains {
            for cookie in chain.cookies {
                cookies[cookie.column, cookie.row] = nil
            }
        }
    }
    
    // MARK: Detect
    
    func detectPossibleSwaps() {
        var set = Set<Swap>()
        
        for row in 0..<NUMROWS {
            for column in 0..<NUMCOLUMNS {
                if let cookie = cookies[column, row] {
                    // is it possible to swap this cookie with the one on the right?
                    if column < NUMCOLUMNS - 1 {
                        // have a cookie in this spot? if there is no title, there is no cookie
                        if let other = cookies[column + 1, row] {
                            // swap them
                            cookies[column, row] = other
                            cookies[column + 1, row] = cookie
                            
                            // is either cookie now part of a chain?
                            if hasChain(column: column + 1, row: row) ||
                                hasChain(column: column, row: row) {
                                    set.insert(Swap(cookieA: cookie, cookieB: other))
                            }
                            
                            // swap them back
                            cookies[column, row] = cookie
                            cookies[column + 1, row] = other
                        }
                    }
                    
                    if row < NUMROWS - 1 {
                        if let other = cookies[column, row + 1] {
                            cookies[column, row] = other
                            cookies[column, row + 1] = cookie
                            
                            // is either cookie now part of a chain?
                            if hasChain(column: column, row: row + 1) ||
                                hasChain(column: column, row: row) {
                                    set.insert(Swap(cookieA: cookie, cookieB: other))
                            }
                            
                            // swap them back
                            cookies[column, row] = cookie
                            cookies[column, row + 1] = other
                        }
                    }
                }
            }
        }
        possibleSwaps = set
    }
    
    private func detectHorizontalMatches() -> Set<Chain> {
        var set = Set<Chain>()
        
        for row in 0..<NUMROWS {
            for var column = 0; column < NUMCOLUMNS - 2; {
                if let cookie = cookies[column, row] {
                    let matchType = cookie.cookieType
                    if cookies[column + 1, row]?.cookieType == matchType &&
                       cookies[column + 2, row]?.cookieType == matchType {
                        let chain = Chain(chainType: .Horizontal)
                        repeat {
                            chain.addCookie(cookies[column, row]!)
                            ++column
                        }
                        while column < NUMCOLUMNS && cookies[column, row]?.cookieType == matchType
                        
                        set.insert(chain)
                        continue
                    }
                }
                ++column
            }
        }
        return set
    }
    
    private func detectVerticalMatches() -> Set<Chain> {
        var set = Set<Chain>()
        
        for column in 0..<NUMCOLUMNS {
            for var row = 0; row < NUMROWS - 2; {
                if let cookie = cookies[column, row] {
                    let matchType = cookie.cookieType
                    
                    if cookies[column, row + 1]?.cookieType == matchType &&
                       cookies[column, row + 2]?.cookieType == matchType {
                        let chain = Chain(chainType: .Vertical)
                        repeat {
                            chain.addCookie(cookies[column, row]!)
                            ++row
                        }
                        while row < NUMROWS && cookies[column, row]?.cookieType == matchType
                        
                        set.insert(chain)
                        continue
                    }
                }
                ++row
            }
        }
        return set
    }
}
