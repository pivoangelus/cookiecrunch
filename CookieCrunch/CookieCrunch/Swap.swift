//
//  Swap.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/21/16.
//  Copyright © 2016 Justin Andros. All rights reserved.
//

import UIKit

struct Swap: CustomStringConvertible, Hashable {
    
    // MARK: Properities
    
    let cookieA: Cookie
    let cookieB: Cookie
    
    // MARK:
    
    init(cookieA: Cookie, cookieB: Cookie) {
        self.cookieA = cookieA
        self.cookieB = cookieB
    }
    
    var description: String {
        return "swap \(cookieA) with \(cookieB)"
    }
    
    // MARK: Hashable
    
    var hashValue: Int {
        return cookieA.hashValue ^ cookieB.hashValue
    }
}

// MARK: Hashable

func ==(lhs: Swap, rhs: Swap) -> Bool {
    return (lhs.cookieA == rhs.cookieA && lhs.cookieB == rhs.cookieB) ||
           (lhs.cookieB == rhs.cookieA && lhs.cookieA == rhs.cookieB)
}