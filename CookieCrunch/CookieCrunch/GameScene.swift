//
//  GameScene.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/16/16.
//  Copyright (c) 2016 Justin Andros. All rights reserved.
//
//  Currently plays in iPhone 5s and lower (need to update the images to 3x)
//

import SpriteKit

class GameScene: SKScene {
    
    // MARK: Properities
    
    var level: Level!
    
    var swipeFromColumn: Int?
    var swipeFromRow: Int?
    
    let TILEWIDTH: CGFloat = 32.0
    let TILEHEIGHT: CGFloat = 36.0
    
    let gameLayer = SKNode()
    let cookiesLayer = SKNode()
    let tilesLayer = SKNode()
    let cropLayer = SKCropNode()
    let maskLayer = SKNode()

    var selectionSprie = SKSpriteNode()
    
    var swipeHandler: ((Swap) -> ())?
    
    // MARK: Sounds
    
    let swapSound = SKAction.playSoundFileNamed("Chomp.wav", waitForCompletion: false)
    let invalidSwapSound = SKAction.playSoundFileNamed("Error.wav", waitForCompletion: false)
    let matchSound = SKAction.playSoundFileNamed("Ka-Ching.wav", waitForCompletion: false)
    let fallingCookieSound = SKAction.playSoundFileNamed("Scrape.wav", waitForCompletion: false)
    let addCookieSound = SKAction.playSoundFileNamed("Drip.wav", waitForCompletion: false)
   
    // MARK:
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder) is not used in this app")
    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        anchorPoint = CGPoint(x: 0.5, y: 0.5)
        
        let background = SKSpriteNode(texture: GAMEATLAS.textureNamed("Background"))
        addChild(background)
        
        addChild(gameLayer)
        
        let layerPosition = CGPoint(
            x: -TILEWIDTH * CGFloat(NUMCOLUMNS) / 2,
            y: -TILEHEIGHT * CGFloat(NUMROWS) / 2
        )
        
        tilesLayer.position = layerPosition
        gameLayer.addChild(tilesLayer)
        
        cookiesLayer.position = layerPosition
        cropLayer.addChild(cookiesLayer)
        
        // uncomment these lines to see is the mask
        //maskLayer.alpha = 0.25
        //cropLayer.addChild(maskLayer)
        
        gameLayer.addChild(cropLayer)
        
        maskLayer.position = layerPosition
        cropLayer.maskNode = maskLayer
        
        swipeFromColumn = nil
        swipeFromRow = nil
        
        // "preload" the font-to-texture to reduce the small delay further
        //SKLabelNode(fontNamed: "GillSans-BoldItalic")
    }
    
    func pointFor(column column: Int, row: Int) -> CGPoint {
        return CGPoint(
            x: CGFloat(column) * TILEWIDTH + TILEWIDTH / 2,
            y: CGFloat(row) * TILEHEIGHT + TILEHEIGHT / 2
        )
    }
    
    func convertPoint(point: CGPoint) -> (success: Bool, column: Int, row: Int) {
        if point.x >= 0 && point.x < CGFloat(NUMCOLUMNS) * TILEWIDTH &&
           point.y >= 0 && point.y < CGFloat(NUMROWS) * TILEHEIGHT {
            return (true, Int(point.x / TILEWIDTH), Int(point.y / TILEHEIGHT))
        } else {
            return (false, 0, 0) // invalid location
        }
    }
    
    func trySwap(horizontal horzDelta: Int, vertical vertDelta: Int) {
        let toColumn = swipeFromColumn! + horzDelta
        let toRow = swipeFromRow! + vertDelta
        
        if toColumn < 0 || toColumn >= NUMCOLUMNS { return }
        if toRow < 0 || toRow >= NUMROWS { return }
        
        if let toCookie = level.cookieAt(column: toColumn, row: toRow) {
            if let fromCookie = level.cookieAt(column: swipeFromColumn!, row: swipeFromRow!) {
                if let handler = swipeHandler {
                    let swap = Swap(cookieA: fromCookie, cookieB: toCookie)
                    handler(swap)
                }
            }
        }
    }
    
    func removeAllCookieSprites () {
        cookiesLayer.removeAllChildren()
    }
    
    // MARK: Animation
    
    func animateSwap(swap: Swap, completion: () -> ()) {
        let spriteA = swap.cookieA.sprite!
        let spriteB = swap.cookieB.sprite!
        
        spriteA.zPosition = 100
        spriteB.zPosition = 90
        
        let duration: NSTimeInterval = 0.3
        
        let moveA = SKAction.moveTo(spriteB.position, duration: duration)
        moveA.timingMode = .EaseOut
        spriteA.runAction(moveA, completion: completion)
        
        let moveB = SKAction.moveTo(spriteA.position, duration: duration)
        moveB.timingMode = .EaseOut
        spriteB.runAction(moveB)
        
        runAction(swapSound)
    }
    
    func animateInvalidSwap(swap: Swap, completion: () -> ()) {
        let spriteA = swap.cookieA.sprite!
        let spriteB = swap.cookieB.sprite!
        
        spriteA.zPosition = 100
        spriteB.zPosition = 90
        
        let duration: NSTimeInterval = 0.2
        
        let moveA = SKAction.moveTo(spriteB.position, duration: duration)
        moveA.timingMode = .EaseOut
        
        let moveB = SKAction.moveTo(spriteA.position, duration: duration)
        moveB.timingMode = .EaseOut
        
        spriteA.runAction(SKAction.sequence([moveA, moveB]), completion: completion)
        spriteB.runAction(SKAction.sequence([moveB, moveA]))
        
        runAction(invalidSwapSound)
    }
    
    func animateMatchedCookies(chains: Set<Chain>, completion: () -> ()) {
        for chain in chains {
            animateScoreForChain(chain)
            for cookie in chain.cookies {
                if let sprite = cookie.sprite {
                    if sprite.actionForKey("removing") == nil {
                        let scaleAction = SKAction.scaleTo(0.1, duration: 0.3)
                        scaleAction.timingMode = .EaseOut
                        sprite.runAction(SKAction.sequence([
                            scaleAction,
                            SKAction.removeFromParent()
                            ]), withKey: "removing")
                    }
                }
            }
        }
        runAction(matchSound)
        runAction(SKAction.waitForDuration(0.3), completion: completion)
    }
    
    func animateFallingCookies(columns: [[Cookie]], completion: () -> ()) {
        var longestDuration: NSTimeInterval = 0
        for array in columns {
            for (i, cookie) in array.enumerate() {
                let newPosition = pointFor(column: cookie.column, row: cookie.row)
                let delay = 0.05 + 0.15 * NSTimeInterval(i)
                let sprite = cookie.sprite!
                let duration = NSTimeInterval(((sprite.position.y - newPosition.y) / TILEHEIGHT) * 0.1)
                longestDuration = max(longestDuration, duration + delay)
                let moveAction = SKAction.moveTo(newPosition, duration: duration)
                moveAction.timingMode = .EaseOut
                sprite.runAction(SKAction.sequence([
                    SKAction.waitForDuration(delay),
                    SKAction.group([moveAction, fallingCookieSound])
                    ]))
            }
        }
        runAction(SKAction.waitForDuration(longestDuration), completion: completion)
    }
    
    func animateNewCookies(columns: [[Cookie]], completion: () -> ()) {
        var longestDuration: NSTimeInterval = 0
        for array in columns {
            let startRow = array[0].row + 1
            for (i, cookie) in array.enumerate() {
                let sprite = SKSpriteNode(texture: SPRITEATLAS.textureNamed(cookie.cookieType.spriteName))
                sprite.position = pointFor(column: cookie.column, row: cookie.row)
                cookiesLayer.addChild(sprite)
                cookie.sprite = sprite
                let delay = 0.1 + 0.2 * NSTimeInterval(array.count - i - 1)
                let duration = NSTimeInterval(startRow - cookie.row) * 0.1
                longestDuration = max(longestDuration, duration + delay)
                let newPosition = pointFor(column: cookie.column, row: cookie.row)
                let moveAction = SKAction.moveTo(newPosition, duration: duration)
                moveAction.timingMode = .EaseOut
                sprite.alpha = 0
                sprite.runAction(SKAction.sequence([
                    SKAction.waitForDuration(delay),
                    SKAction.group([
                        SKAction.fadeInWithDuration(0.05),
                        moveAction,
                        addCookieSound
                        ])
                    ]))
            }
        }
        runAction(SKAction.waitForDuration(longestDuration), completion: completion)
    }
    
    func animateScoreForChain(chain: Chain) {
        // figure out waht the midpoint of the chain is
        let firstSprite = chain.firstCookie().sprite!
        let lastSprite = chain.lastCookie().sprite!
        let centerPoint = CGPoint(
            x: (firstSprite.position.x + lastSprite.position.x) / 2,
            y: (firstSprite.position.y + lastSprite.position.y) / 2
        )
        
        // add a label for the score that slowly floats up
        let scoreLabel = SKLabelNode(fontNamed: "GillSans-BoldItalic")
        scoreLabel.fontSize = 16
        scoreLabel.text = String(format: "%ld", chain.score)
        scoreLabel.position = centerPoint
        scoreLabel.zPosition = 200
        cookiesLayer.addChild(scoreLabel)
        
        let moveAction = SKAction.moveBy(CGVector(dx: 0, dy: 3), duration: 0.7)
        moveAction.timingMode = .EaseOut
        scoreLabel.runAction(SKAction.sequence([
            moveAction,
            SKAction.removeFromParent()
            ]))
    }
    
    func animateGameOver(completion: () -> ()) {
        let action = SKAction.moveBy(CGVector(dx: 0, dy: -size.height), duration: 0.3)
        action.timingMode = .EaseIn
        gameLayer.runAction(action, completion: completion)
    }
    
    func animateBeginGame(completion: () -> ()) {
        gameLayer.position = CGPoint(x: 0, y: size.height)
        let action = SKAction.moveBy(CGVector(dx: 0, dy: -size.height), duration: 0.3)
        action.timingMode = .EaseOut
        gameLayer.runAction(action, completion: completion)
    }
    
    // MARK: Selection
    
    func showSelection(cookie: Cookie) {
        if selectionSprie.parent != nil {
            selectionSprie.removeFromParent()
        }
        
        if let sprite = cookie.sprite {
            let texture = SPRITEATLAS.textureNamed(cookie.cookieType.highlightedSpriteName)
            selectionSprie.size = texture.size()
            selectionSprie.runAction(SKAction.setTexture(texture))
            
            sprite.addChild(selectionSprie)
            selectionSprie.alpha = 1.0
        }
    }
    
    func hideSelection() {
        selectionSprie.runAction(SKAction.sequence([
            SKAction.fadeOutWithDuration(0.3),
            SKAction.removeFromParent()
            ]))
    }
    
    // MARK: Add
    
    func addSpritesForCookies(cookies: Set<Cookie>) {
        for cookie in cookies {
            let sprite = SKSpriteNode(texture: SPRITEATLAS.textureNamed(cookie.cookieType.spriteName))
            sprite.position = pointFor(column: cookie.column, row: cookie.row)
            cookiesLayer.addChild(sprite)
            cookie.sprite = sprite
            
            // give each cookie sprite a small, random delay, then fade in
            sprite.alpha = 0
            sprite.xScale = 0.5
            sprite.yScale = 0.5
            
            sprite.runAction(SKAction.sequence([
                SKAction.waitForDuration(0.25, withRange: 0.5),
                SKAction.group([
                    SKAction.fadeInWithDuration(0.25),
                    SKAction.scaleTo(1.0, duration: 0.25)
                    ])
                ]))
        }
    }
    
    func addTiles() {
        for row in 0..<NUMROWS {
            for column in 0..<NUMCOLUMNS {
                if let _ = level.tileAt(column: column, row: row) {
                    let tileNode = SKSpriteNode(texture: GRIDATLAS.textureNamed("MaskTile"))
                    tileNode.position = pointFor(column: column, row: row)
                    maskLayer.addChild(tileNode)
                }
            }
        }
        
        for row in 0...NUMROWS {
            for column in 0...NUMCOLUMNS {
                let topLeft = (column > 0) && (row < NUMROWS) && level.tileAt(column: column - 1, row: row) != nil
                let bottomleft = (column > 0) && (row > 0) && level.tileAt(column: column - 1, row: row - 1) != nil
                let topRight = (column < NUMCOLUMNS) && (row < NUMROWS) && level.tileAt(column: column, row: row) != nil
                let bottomRight = (column < NUMCOLUMNS) && (row > 0) && level.tileAt(column: column, row: row - 1) != nil
                
                // tiles are named from 0 to 15, according to the bitmask that is made by combining these four values
                let value = Int(topLeft) | Int(topRight) << 1 | Int(bottomleft) << 2 | Int (bottomRight) << 3
                
                // values 0 (no tiles), 6 and 9 (two opposite tiles) are not drawn
                if value != 0 && value != 6 && value != 9 {
                    let name = String(format: "Tile_%ld", value)
                    let tileNode = SKSpriteNode(texture: GRIDATLAS.textureNamed(name))
                    var point = pointFor(column: column, row: row)
                    point.x -= TILEWIDTH / 2
                    point.y -= TILEHEIGHT / 2
                    tileNode.position = point
                    tilesLayer.addChild(tileNode)
                }
            }
        }
    }
    
    // MARK: Touches
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first!
        let location = touch.locationInNode(cookiesLayer)
        let (success, column, row) = convertPoint(location)
        if success {
            if let cookie = level.cookieAt(column: column, row: row) {
                showSelection(cookie)
                swipeFromColumn = column
                swipeFromRow = row
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if swipeFromColumn == nil { return }
        
        let touch = touches.first!
        let location = touch.locationInNode(cookiesLayer)
        
        let (success, column, row) = convertPoint(location)
        if success {
            var horzDelta = 0, vertDelta = 0
            if column < swipeFromColumn! {          // swipe left
                horzDelta = -1
            } else if column > swipeFromColumn! {   // swipe right
                horzDelta = 1
            } else if row < swipeFromRow! {         // swipe down
                vertDelta = -1
            } else if row > swipeFromRow! {         // swipe up
                vertDelta = 1
            }
            
            if horzDelta != 0 || vertDelta != 0 {
                trySwap(horizontal: horzDelta, vertical: vertDelta)
                hideSelection()
                swipeFromColumn = nil
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if selectionSprie.parent != nil && swipeFromColumn != nil {
            hideSelection()
        }
        swipeFromColumn = nil
        swipeFromRow = nil
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        touchesEnded(touches!, withEvent: event)
    }
    
}
