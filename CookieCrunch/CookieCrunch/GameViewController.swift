//
//  GameViewController.swift
//  CookieCrunch
//
//  Created by Justin Andros on 2/16/16.
//  Copyright (c) 2016 Justin Andros. All rights reserved.
//

import UIKit
import SpriteKit
import AVFoundation

class GameViewController: UIViewController {

    // MARK: Properities
    
    var level: Level!
    var scene: GameScene!
    
    var movesLeft = 0
    var score = 0
    
    var tapGestureRecognizer: UITapGestureRecognizer!
    
    // MARK: IBOutlet
    
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var movesLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var gameNotificationImageView: UIImageView!
    @IBOutlet weak var shuffleButton: UIButton!
    
    // MARK: AV Player
    
    lazy var backgroundMusic: AVAudioPlayer = {
        do {
            let url = NSBundle.mainBundle().URLForResource("Mining by Moonlight", withExtension: "mp3")
            let player = try? AVAudioPlayer(contentsOfURL: url!)
            player!.numberOfLoops = -1
            return player!
        }
    }()
    
    // MARK:
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // configure view
        let skView = view as! SKView
        skView.multipleTouchEnabled = false
        
        // create and configure the scene
        scene = GameScene(size: skView.bounds.size)
        scene.scaleMode = .AspectFill
        
        shuffleButton.hidden = true
        gameNotificationImageView.hidden = true
        
        level = Level(filename: "Level_1")
        scene.level = level
        scene.addTiles()
        scene.swipeHandler = handleSwipe
        
        // present the scene
        skView.presentScene(scene)
        
        backgroundMusic.play()
        beginGame()
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.AllButUpsideDown
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func updateLabels() {
        targetLabel.text = String(format: "%ld", level.targetScore)
        movesLabel.text = String(format: "%ld", movesLeft)
        scoreLabel.text = String(format: "%ld", score)
    }
    
    func beginGame() {
        movesLeft = level.maximumMoves
        score = 0
        updateLabels()
        //level.resetComboMultiplier()
        scene.animateBeginGame() {
            self.shuffleButton.hidden = false
        }
        shuffle()
    }
    
    func gameOver() {
        shuffleButton.hidden = true
        gameNotificationImageView.hidden = false
        scene.userInteractionEnabled = false
        
        scene.animateGameOver() {
            self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "hideGameOver")
            self.view.addGestureRecognizer(self.tapGestureRecognizer)
        }
    }
    
    func hideGameOver() {
        view.removeGestureRecognizer(tapGestureRecognizer)
        tapGestureRecognizer = nil
        
        gameNotificationImageView.hidden = true
        scene.userInteractionEnabled = true
        
        beginGame()
    }
    
    func beginNextTurn() {
        level.resetComboMultiplier()
        level.detectPossibleSwaps()
        view.userInteractionEnabled = true
        decrementMoves()
    }
    
    func shuffle() {
        scene.removeAllCookieSprites()
        let newCookies = level.shuffle()
        scene.addSpritesForCookies(newCookies)
    }
    
    func handleSwipe(swap: Swap) {
        view.userInteractionEnabled = false
        
        if level.isPossibleSwap(swap) {
            level.performSwap(swap)
            scene.animateSwap(swap, completion: handleMatches)
        } else {
            scene.animateInvalidSwap(swap) {
                self.view.userInteractionEnabled = true
            }
        }
    }
    
    func handleMatches() {
        let chains = level.removeMatches()
        if chains.count == 0 {
            beginNextTurn()
            return
        }
        
        scene.animateMatchedCookies(chains) {
            for chain in chains {
                self.score += chain.score
            }
            self.updateLabels()
            let columns = self.level.fillHoles()
            self.scene.animateFallingCookies(columns) {
                let columns = self.level.topOffCookies()
                self.scene.animateNewCookies(columns) {
                    print("cookies removed and board topped off")
                    self.handleMatches()
                }
            }
        }
    }
    
    func decrementMoves() {
        --movesLeft
        updateLabels()
        
        if score >= level.targetScore {
            gameNotificationImageView.image = UIImage(named: "LevelComplete")
            gameOver()
        } else if movesLeft <= 0 {
            gameNotificationImageView.image = UIImage(named: "GameOver")
            gameOver()  
        }
    }
    
    // MARK: IBAction
    
    @IBAction func shuffleButtonPressed(_: AnyObject) {
        shuffle()
        decrementMoves()
    }
}
